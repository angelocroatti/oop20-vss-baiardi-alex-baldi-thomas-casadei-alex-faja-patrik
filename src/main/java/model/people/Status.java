package model.people;

/**
 * Represents the possibly status of a person.
 */
public enum Status {
    /**
     * Person status.
     */
    SUSCEPTIBLE, INFECTED, ILL, RECOVERED;

}
