package view;

/**
 * Launcher class.
 */
public final class Launcher {

    private Launcher() {
    };

    /**
     * 
     * @param args
     */
    public static void main(final String[] args) {
        MainView.main(args);
    }

}
